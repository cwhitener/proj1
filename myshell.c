/*
	myshell.c

	Chase Whitener
	FSUID: ccw09c
	CSID: whitener

	Project 1, Linux shell written in C
	COP4610 - Operating Systems
	FSU - Spring 2013
*/

/* includes */
#include <stdio.h>
#include <stdlib.h>

#include <pwd.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>

#ifndef LINE_MAX
#define LINE_MAX 65535
#endif
#ifndef PATH_MAX
#define PATH_MAX 1000
#endif

const char *WHITESPACE = " \n\r\f\t\v";

/* Create our command structure as a type */
typedef struct {
	size_t	argc;
	size_t	extargc;
	char	**argv;
	char	**extargv;
	char	*path;
	char	*ifile;
	char	*ofile;
} Command;

int DBUG(void) { return 0; }
void command_add_arg( Command *c, const char *val );
void command_add_xarg( Command *c, const char *val );
int  command_build_xargs( Command *c );
void command_change_arg( Command *c, size_t i, const char *val );
void command_destroy( Command *c );
void command_dump( const Command *c );
void command_init( Command *c );
void command_set_path( Command *c, const char *val );
void command_set_ifile( Command *c, const char *val );
void command_set_ofile( Command *c, const char *val );
void get_cwd( char *cwd, const size_t size );
void handle_cd( Command *c );
void handle_command( const char *path ); /* run command */
void handle_echo( Command *c );
void handle_exec( Command *c );
int is_directory( const char *path );
int is_file( const char *path );
void show_prompt( void ); /* displays the user prompt */

int main(void) {
	char buffer[LINE_MAX];
	char *command;

	while ( 1 ) {
		show_prompt();
		if ( fgets(buffer,LINE_MAX,stdin) == NULL ) {
			fputs( "Error reading from STDIN.  Exiting.\n", stderr );
			return 1;
		}
		command = strtok( buffer, WHITESPACE );
		if ( command == NULL ) {
			continue;
		}
		if ( strcmp(command, "exit") == 0 ) {
			break;
		}
		else {
			handle_command( command );
		}
	}
	return 0;
}

/* -----------------------------------------------------------------------------
 command_add_arg( Command *c, const char *val )
 -- resize our dynamic array. allocate enough space in the slot to store val
	and copy val's contents into it
	argc is the count of actual arguments we have.  0 - (argc-1) are real args
	argc itself will always be set to null so exec knows what to do
----------------------------------------------------------------------------- */
void command_add_arg( Command *c, const char *val ) {
	char **temp;
	char *tmp;
	if ( NULL == c ) return;
	if ( NULL == val ) return;
	if ( strlen(val)==0 ) return;

	if ( (temp = realloc( c->argv, sizeof(char*)*(c->argc+2))) == NULL ) {
		fputs("Failed to re-size argument array.\n", stderr);
		command_destroy( c );
		exit(1);
	}
	c->argv = temp;
	c->argc++;
	temp = NULL;

	if ( (tmp = malloc( sizeof(char)*(strlen(val)+1) ))==NULL) {
		fputs( "Failed to allocate memory for string argument.\n", stderr );
		command_destroy( c );
		exit(1);
	}
	strcpy( tmp, val );
	c->argv[c->argc - 1] = tmp;
	c->argv[c->argc] = NULL;
	tmp = NULL;
}

/* -----------------------------------------------------------------------------
 command_add_xarg( Command *c, const char *val )
 -- resize our dynamic array. allocate enough space in the slot to store val
	and copy val's contents into it
	argc is the count of actual arguments we have.  0 - (argc-1) are real args
	argc itself will always be set to null so exec knows what to do
----------------------------------------------------------------------------- */
void command_add_xarg( Command *c, const char *val ) {
	char **temp;
	char *tmp;
	if ( NULL == c ) return;
	if ( NULL == val ) return;
	if ( strlen(val)==0 ) return;

	if ( (temp = realloc( c->extargv, sizeof(char*)*(c->extargc+2))) == NULL ) {
		fputs("Failed to re-size external argument array.\n", stderr);
		command_destroy( c );
		exit(1);
	}
	c->extargv = temp;
	c->extargc++;
	temp = NULL;

	if ( (tmp = malloc( sizeof(char)*(strlen(val)+1) ))==NULL) {
		fputs( "Failed to allocate memory for string argument.\n", stderr );
		command_destroy( c );
		exit(1);
	}
	strcpy( tmp, val );
	c->extargv[c->extargc - 1] = tmp;
	c->extargv[c->extargc] = NULL;
	tmp = NULL;
}

/* -----------------------------------------------------------------------------
 command_change_arg( Command *c, size_t i, const char *val )
 -- set the value of a particular argument to something different
----------------------------------------------------------------------------- */
int command_build_xargs( Command *c ) {
	size_t i = 0;
	if ( DBUG() ) printf( " ->DBUG: build_xargs()!\n" );
	if ( DBUG() ) command_dump(c);

	if ( NULL == c ) return 0;
	command_add_xarg( c, c->path );
	if ( NULL == c->ifile || strlen(c->ifile) < 1 ) {
		for ( i = 0; i < c->argc; ++i ) {
			command_add_xarg( c, c->argv[i] );
		}
	}
	return 1;
}

/* -----------------------------------------------------------------------------
 command_change_arg( Command *c, size_t i, const char *val )
 -- set the value of a particular argument to something different
----------------------------------------------------------------------------- */
void command_change_arg( Command *c, size_t i, const char *val ) {
	char *temp;
	if ( NULL == c ) return;
	if ( i >= c->argc ) return;
	if ( NULL == c->argv ) return;
	if ( (temp = realloc( c->argv[i], sizeof(char)*(strlen(val)+1)))==NULL ) {
		fputs( "Failed to adjust the size available for argument.\n", stderr );
		command_destroy( c );
		exit(1);
	}
	strcpy( temp, val );
	c->argv[i] = temp;
	temp = NULL;
}

/* -----------------------------------------------------------------------------
 command_destroy( Command *c )
 -- Clean up any allocated memory for our Command struct
----------------------------------------------------------------------------- */
void command_destroy( Command *c ) {
	size_t i;
	if ( NULL == c ) return;
	if ( NULL != c->path ) free(c->path);
	if ( NULL != c->ifile ) free( c->ifile );
	if ( NULL != c->ofile ) free( c->ofile );
	if ( NULL != c->argv ) {
		for ( i = 0; i < c->argc; ++i ) {
			if ( NULL != c->argv[i] ) free(c->argv[i]);
			c->argv[i] = NULL;
		}
		free(c->argv);
	}
	if ( NULL != c->extargv ) {
		for ( i = 0; i < c->extargc; ++i ) {
			if ( NULL != c->extargv[i] ) free(c->extargv[i]);
			c->extargv[i] = NULL;
		}
		free(c->extargv);
	}
	c->path = NULL;
	c->ifile = NULL;
	c->ofile = NULL;
	c->argv = NULL;
	c->argc = 0;
	c->extargc = 0;
}

/* -----------------------------------------------------------------------------
 command_dump( const Command *c )
 -- Dump out the information stored in our Command.  Helps to debug
----------------------------------------------------------------------------- */
void command_dump( const Command *c ) {
	size_t i;
	if ( NULL == c ) return;
	printf( "\n\n" );
	printf( "------------------------------------\n" );
	printf( " Command: `%s`\n", c->path );
	printf( "  Input File: %s\n  Output File: %s\n", c->ifile, c->ofile );
	printf( "  Num Args: %lu\n", c->argc );
	for (i=0; i <= c->argc; ++i ) {
		printf( "   arg[%lu] = %s\n", i, c->argv[i] );
	}
	printf( "  Num External Command Args: %lu\n", c->extargc );
	for (i=0; i <= c->extargc; ++i ) {
		printf( "   arg[%lu] = %s\n", i, c->extargv[i] );
	}
	printf( "------------------------------------\n\n" );
}

/* -----------------------------------------------------------------------------
 command_init( Command *c )
 -- Initialize our Command struct and make sure we always have one argument
	set to NULL
----------------------------------------------------------------------------- */
void command_init( Command *c ) {
	if ( NULL == c ) return;
	c->path = NULL;
	c->ifile = NULL;
	c->ofile = NULL;
	c->argv = NULL;
	c->extargv = NULL;
	c->argc = 0;
	c->extargc = 0;
	if ( (c->argv=malloc(sizeof(char*)))==NULL ) {
		fputs("failed to allocate memory\n", stderr);
		exit(1);
	}
	if ( (c->extargv=malloc(sizeof(char*)))==NULL ) {
		command_destroy( c );
		fputs("failed to allocate memory\n", stderr);
		exit(1);
	}
	c->argv[0] = NULL;
	c->extargv[0] = NULL;
}

/* -----------------------------------------------------------------------------
 command_set_path( Command *c, const char *val )
 -- Set the path (command) portion to the value supplied.  Properly allocate 
 enough space to do so.  If no space can be allocated, destroy the Command
 as best we can and exit.
----------------------------------------------------------------------------- */
void command_set_path( Command *c, const char *val ) {
	char *temp;
	if ( NULL == c ) return;
	if ( NULL == val ) return;
	if ( strlen(val) == 0 ) return;

	if ( DBUG() ) printf( "-> DEBUG: command_set_path(): length: %lu, val: %s\n", strlen(val), val);
	if ( (temp = realloc( c->path, sizeof(char)*(strlen(val)+1) ))==NULL) {
		fputs( "Failed to allocate memory for path.\n", stderr );
		command_destroy( c );
		exit(1);
	}
	strcpy( temp, val );
	c->path = temp;
	if ( DBUG() ) printf( "-> DEBUG: command_set_path(): new length: %lu, val: %s\n", strlen(c->path), c->path);
}

/* -----------------------------------------------------------------------------
 command_set_ifile( Command *c, const char *val )
 -- Set the ifile portion to the value supplied.  Properly allocate
 enough space to do so.  If no space can be allocated, destroy the Command
 as best we can and exit.
----------------------------------------------------------------------------- */
void command_set_ifile( Command *c, const char *val ) {
	char *temp;
	if ( NULL == c ) return;
	if ( NULL == val ) return;
	if ( strlen(val)==0 ) return;

	if ( (temp = realloc( c->ifile, sizeof(char)*(strlen(val)+1)))==NULL ) {
		fputs( "Failed to adjust the size available for input file.\n", stderr );
		command_destroy( c );
		exit(1);
	}
	strcpy( temp, val );
	c->ifile = temp;
}

/* -----------------------------------------------------------------------------
 command_set_ofile( Command *c, const char *val )
 -- Set the path (command) portion to the value supplied.  Properly allocate
 enough space to do so.  If no space can be allocated, destroy the Command
 as best we can and exit.
----------------------------------------------------------------------------- */
void command_set_ofile( Command *c, const char *val ) {
	char *temp;
	if ( NULL == c ) return;
	if ( NULL == val ) return;
	if ( strlen(val)==0 ) return;

	if ( (temp = realloc( c->ofile, sizeof(char)*(strlen(val)+1)))==NULL ) {
		fputs( "Failed to adjust the size available for output file.\n", stderr );
		command_destroy( c );
		exit(1);
	}
	strcpy( temp, val );
	c->ofile = temp;
}

/* -----------------------------------------------------------------------------
 get_cwd( char *cwd, const size_t size )
 -- figure out what the CWD is and fill in the provided pointer.  caller must
 	free the pointer
----------------------------------------------------------------------------- */
void get_cwd( char *cwd, const size_t size ) {
	char *ptr = NULL;
	
	ptr = getcwd( cwd, size );
	if ( NULL == ptr ) {
		fputs( "Error trying to grab the CWD.\n", stderr );
		exit(1);
	}
}

/* -----------------------------------------------------------------------------
 handle_command( char *path, char *buffer )
 -- figure out what command we're supposed to try and do it
 parse out the buffer as the arguments to the command with an extra NULL
----------------------------------------------------------------------------- */
void handle_command( const char *path ) {
	Command com;
	char *token;
	if ( path == NULL ) return;
	if ( strlen(path)==0 ) return;
	/* setup a new Command */
	command_init( &com );

	if ( DBUG() ) printf( "-> DEBUG: handle_command(): %s\n\n", path );
	command_set_path( &com, path );
	
	token = strtok( NULL, WHITESPACE );
	while ( token != NULL ) {
		if ( strlen(token) > 0 ) {
			if ( strcmp(token, ">") == 0 ) {
				token = strtok( NULL, WHITESPACE );
				if ( token == NULL ) {
					fputs( "Invalid output file.\n", stderr );
					command_destroy( &com );
					return;
				}
				command_set_ofile( &com, token );
			}
			else if ( strcmp(token, "<") == 0 ) {
				token = strtok( NULL, WHITESPACE );
				if ( token == NULL ) {
					fputs( "Invalid input file.\n", stderr );
					command_destroy( &com );
					return;
				}
				command_set_ifile( &com, token );
			}
			else {
				if ( strlen(token) > 1 && token[0]=='$' ) {
					char *temp = NULL;
					temp = getenv( token+1 );
					if ( temp == NULL ) {
						printf( "%s: Undefined variable.\n", token );
						command_destroy( &com );
						return;
					}
					command_add_arg( &com, temp );
				}
				else {
					command_add_arg( &com, token );
				}
			}
		}
		token = strtok( NULL, WHITESPACE );
	}
	if ( strcmp(path,"echo")==0 ) {
		handle_echo( &com );
	}
	else if ( strcmp(path,"cd")==0 ) {
		handle_cd( &com );
	}
	else {
		handle_exec( &com );
	}
	/* DUMP contents */
	/* command_dump( &com ); */
	
	command_destroy( &com );
}

/* ----------------------------------------------------------------------------
 handle_cd( Command *c )
 -- Mimic the "cd" command on your normal shell
----------------------------------------------------------------------------- */
void handle_cd( Command *c ) {
	char cwd[PATH_MAX];
	char path[PATH_MAX];
	char *temp = NULL;
	int ret = 0;
	if ( NULL == c ) return;

	memset(cwd,'\0',PATH_MAX);
	memset(path,'\0',PATH_MAX);
	get_cwd(cwd,PATH_MAX);
	strcpy(path,cwd);

	if ( c->argc > 1 ) {
		printf( "cd: Too many arguments.\n" );
		return;
	}

	if ( 0 == c->argc ) {
		temp = getenv( "HOME" );
		if ( NULL == temp ) {
			fputs( "Error grabbing $HOME variable.\n", stderr );
			exit(1);
		}
		strcpy( path, temp );
		temp = NULL;
	}
	else if ( 1 == c->argc ) {
		temp = c->argv[0];
		if ( '/' == temp[0] ) {
			strcpy( path, temp );
		}
		else {
			strcat(path, "/");
			strcat(path, temp);
		}
	}
	temp = NULL;

	if ( 1 != is_directory(path) ) {
		printf( "%s: No such file or directory.\n", path );
		return;
	}

	ret = chdir( path );
	if ( NULL != temp ) {
		free(temp);
		temp = NULL;
	}

	if ( 0 == ret ) return;
	fputs( "Error changing directories.\n", stderr );
	exit(1);
}

/* ----------------------------------------------------------------------------
 handle_echo( Command *c )
 -- Mimic the "echo" command on your normal shell
----------------------------------------------------------------------------- */
void handle_echo( Command *c ) {
	size_t i;
	if ( NULL == c ) return;

	for ( i = 0; i < c->argc; ++i ) {
		if ( i > 0 ) printf( " " );
		printf( "%s", c->argv[i] );
	}
	printf("\n");
}

/* ----------------------------------------------------------------------------
 handle_exec( Command *c )
 -- handle all commands we don't mimic with execv
----------------------------------------------------------------------------- */
void handle_exec( Command *c ) {
	int status = 0;
	pid_t pid;
	char path[PATH_MAX];
	char env[LINE_MAX];
	memset( path, '\0', PATH_MAX );
	memset( env, '\0', LINE_MAX );

	if ( NULL == c ) return;
	if ( NULL == strchr(c->path, '/') ) {
		char *temp = getenv( "PATH" );
		strcpy( env, temp );
		temp = strtok( env, ":\n" );
		while ( temp != NULL ) {
			strcpy( path, temp );
			strcat( path, "/" );
			strcat( path, c->path );
			if ( is_file(path) ) {
				break;
			}
			temp = strtok( NULL, ":\n");
		}
	}
	else {
		strcpy( path, c->path );
	}
	if ( !is_file( path ) ) {
		printf( "%s: Command not found.\n", c->path );
		return;
	}
	if ( 0== command_build_xargs( c ) ) return;

	/*
	command_dump( c );
	return;
	*/

	pid = fork();
	if ( pid==0 ) {
		int good = 1;
		/* Only child executes */
		if ( NULL != c->ofile ) {
			status = open(c->ofile, O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR);
			if ( status != -1 ) {
				dup2(status,1);
				close(status);
			}
		}
		if ( NULL != c->ifile ) {
			if ( !is_file( c->ifile ) ) {
				fprintf( stderr, "%s: No such file or directory.\n", c->ifile );
				good = 0;
			}
			if ( good ) {
				status = open( c->ifile, O_RDONLY );
				if ( status == -1 ) {
					fprintf( stderr, "%s: No such file or directory.\n", c->ifile );
					good = 0;
				}
				else {
					dup2(status,0);
					close(status);
				}
			}
		}
		if ( good )
			execv( path, c->extargv );
		exit(0);
	}
	else if ( pid < 0 ) {
		/* Failed for fork */
		fputs( "Failed to fork.  Please try again.\n", stderr );
		return;
	}
	else {
		/* only the parent. */
		wait( &status );
		return;
	}
	return;
}

/* ----------------------------------------------------------------------------
 is_directory( const char* path )
 -- if path given is a directory, return 1.  else return 0.
----------------------------------------------------------------------------- */
int is_directory( const char *path ) {
	struct stat fstats;
	int ret = 0;
	if ( NULL==path ) return 0;
	if ( strlen(path) < 1 ) return 0;

	ret = stat( path, &fstats );
	if ( 0 != ret ) {
		return 0;
	}
	if( S_ISDIR(fstats.st_mode) ) return 1;
	return 0;
}

/* ----------------------------------------------------------------------------
 is_file( const char* path )
 -- if path given is a directory, return 1.  else return 0.
----------------------------------------------------------------------------- */
int is_file( const char *path ) {
	struct stat fstats;
	int ret = 0;
	if ( NULL==path ) return 0;
	if ( strlen(path) < 1 ) return 0;

	ret = stat( path, &fstats );
	if ( 0!=ret ) return 0;
	if( S_ISREG(fstats.st_mode) ) return 1;
	return 0;
}

/* ----------------------------------------------------------------------------
 show_prompt()
 -- Shows a prompt  USER@myshell:CWD>
 We do not rely on the ENV variables, but rather use C libraries
----------------------------------------------------------------------------- */
void show_prompt( void ) {
	char cwd[PATH_MAX];
	struct passwd *p;
	memset( cwd, '\0', PATH_MAX);

	/* since getlogin() can easily be fooled... */
	p = getpwuid( getuid() );
	if ( p == NULL ) {
		fputs( "Error grabbing current username.\n", stderr );
		exit(1);
	}
	get_cwd(cwd, PATH_MAX);

	printf( "%s@myshell:%s> ", p->pw_name, cwd );
	p = NULL;
}
